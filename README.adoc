= Apache Tika CLARIN image
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Docker image for creating Apache Tika servers.
Processes are managed by supervisord and log aggregation is handled with fluentd (inherited from https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[alpine supervisor base image]).

The image is meant to be used directly as a service. Its containers expose an Apache Tika server (on port 9998) which responds to all IPs inside the same Docker network. To expose the service to the host machine, a Docker port mapping should be added when running this container.

== Configuration

=== Inherited configuration

Configuration inherited from the https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[alpine supervisor base image] via the https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-java-base[alpine supervisor Java base image]

=== Configuring Apache Tika (overriding default configuration)
Supply tika-config.xml configurations via a docker mount directly overriding /srv/apache-tika/tika-config.xml. Do this either in your compose configuration or in the dockerfile which extends this image.


